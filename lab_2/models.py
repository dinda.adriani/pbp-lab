from django.db import models

# Create your models here.

class Note (models.Model):
    to = models.CharField(max_length=32)
    sender = models.CharField(max_length=32)
    title = models.CharField(max_length=32)
    message = models.TextField()