from django.urls import path
from .views import index, xml, json
from .models import Note

urlpatterns = [
    path('', index, name='index'), # ke views bagian index
    path('xml', xml, name='xml'),
    path('json', json, name='json')
]