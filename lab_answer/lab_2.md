1. Apakah perbedaan antara JSON dan XML?

Sebelum mengetahui apa perbedaan antara JSON dan XML, kita harus mengerti terlebih dahulu apa perbedaan antara JSON dan XML.
JSON merupakan sebuah format untuk menyimpan data yang nantinya dapat dipindah atau ditransfer, sedangkan
XML adalah markup language yang digunakan untuk menyimpan data dengan format yang gampang dibaca oleh manusia dan dapat dipindah atau ditransfer.

Keduanya sama-sama digunakan untuk menyimpan informasi atau data dan dapat diakses. 

Perbedaan keduanya kebanyakan terletak pada sifat masing-masing. Sebagai contoh, JSON merepresentasikan data dengan object notation, sedangkan
XML dengan markup language dan menggunakan tag-tag. Jika JSON hanya mensupport data type string dan number,
XML mensupport banyak tipe data seperti number, text, images, chart, graphs, dan lain-lain.
Kita tidak bisa memberi komentar pada JSON, namun kita bisa memberi komentar pada XML. Pengambilan nilai
dari JSON juga cenderung lebih mudah daripada XML. Selain itu perbedaan keduanya juga terletak pada keamanan, di mana 
XML lebih aman daripada JSON. Perbedaan lainnya juga terdapat pada orientasi keduanya, di mana JSON berorientasi pada
data sedangkan XML pada dokumen.


2. Apakah perbedaan antara HTML dan XML?

Perbedaan yang paling mencolok antara XML dan HTML adalah, seperti yang telah diketahui, XML fokus untuk menyimpan dan mentransportasikan data, sedangkan
HTML lebih kepada struktur web page dan menampilkan data pada web page. Keduanya sama-sama memakai tag, namun HTML 
memiliki tag yang sudah didefinisikan, sedangkan pada XML kita membuat tag kita sendiri.

Daftar Pustaka:
- GeeksforGeeks. (2019, February 19). Difference between JSON and XML. https://www.geeksforgeeks.org/difference-between-json-and-xml/
- Walker, A. (2021, August 27). JSON vs XML: What’s the Difference? Guru99. https://www.guru99.com/json-vs-xml-difference.html\
- A. (2021, March 22). General Data Protection Regulation(GDPR) Guidelines BYJU’S. BYJUS. https://byjus.com/free-ias-prep/difference-between-xml-and-html/
- Martin, M. (2021, August 27). Difference between XML and HTML. Guru99. https://www.guru99.com/xml-vs-html-difference.html