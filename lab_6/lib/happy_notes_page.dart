// ignore_for_file: unnecessary_const

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_6/drawer.dart';

class NotesPage extends StatelessWidget {
  NotesPage({Key? key}) : super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {
            if (_scaffoldKey.currentState!.isDrawerOpen == false) {
              _scaffoldKey.currentState!.openDrawer();
            } else {
              _scaffoldKey.currentState!.openEndDrawer();
            }
          },
        ),
        title: const Text("Temenin Isoman"),
      ),
      body: Scaffold(
          key: _scaffoldKey,
          drawer: const MainDrawer(),
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.only(
                  top: 20, bottom: 20, left: 10, right: 10),
              margin: const EdgeInsets.all(8),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                        "Send your Happy Notes to cheering up those who in Isoman :)!"),
                    const SizedBox(
                      height: 10,
                    ),
                    GridView.count(
                      shrinkWrap: true,
                      controller: _scrollController,
                      crossAxisCount: 1,
                      crossAxisSpacing: 8,
                      mainAxisSpacing: 8,
                      children: [
                        Container(
                          height: 100,
                          child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)),
                              child: Container(
                                padding: const EdgeInsets.all(16),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: const <Widget>[
                                    const ListTile(
                                      title: Text(
                                        'Be happy',
                                        textAlign: TextAlign.center,
                                      ),
                                      subtitle: Text('from: lalalilili'),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam iaculis et dolor at scelerisque. Cras laoreet luctus ante, vel interdum urna tempus eget. Nulla augue justo, laoreet ac erat iaculis")
                                  ],
                                ),
                              )),
                        ),
                        Container(
                          height: 100,
                          child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)),
                              child: Container(
                                padding: const EdgeInsets.all(16),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: const <Widget>[
                                    const ListTile(
                                      title: Text(
                                        'Be happy',
                                        textAlign: TextAlign.center,
                                      ),
                                      subtitle: Text('from: lalalilili'),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam iaculis et dolor at scelerisque. Cras laoreet luctus ante, vel interdum urna tempus eget. Nulla augue justo, laoreet ac erat iaculis")
                                  ],
                                ),
                              )),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
