import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_6/happy_notes_page.dart';
import 'package:lab_6/home_page.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          ListTile(
            title: const Text('Home'),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const MyHomePage(title: 'Temenin Isoman');
              }));
            },
          ),
          ListTile(
            title: const Text('Deteksi Mandiri'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('Checklist'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('Happy Notes'),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return NotesPage();
              }));
            },
          ),
          ListTile(
            title: const Text('Tips and Trick'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('Symptoms and Medicines'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('Bed Capacity Information'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('Emergency Contact'),
            onTap: () {},
          )
        ],
      ),
    );
  }
}
