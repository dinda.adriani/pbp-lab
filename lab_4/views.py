from django.shortcuts import render, redirect
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/admin/login/')
def index(request):
   notes = Note.objects.all()
   response = {'notes': notes}
   return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
   context ={}
   # create object of note
   form = NoteForm(request.POST or None, request.FILES or None)
   
   # check if form data is valid
   if form.is_valid():
      # save the form data to model
      form.save()
      if request.method == "POST" :
         return redirect('/lab-4/') 
      # supaya habis submit balik lagi ke friend list

   context['form']= form
   return render(request, "lab4_forms.html", context)

def note_list(request):
   notes = Note.objects.all()
   response = {'notes': notes}
   return render(request, 'lab4_note_list.html', response)   