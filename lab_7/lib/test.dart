import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7/widget/drawer.dart';

class TestPage extends StatefulWidget {
  @override
  State<TestPage> createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: MainDrawer(),
      body: SafeArea(
        bottom: false,
        top: false,
        child: Container(
          padding: new EdgeInsets.only(
            top: 92,
            left: 16,
            right: 16,
          ),
          decoration: BoxDecoration(color: Colors.grey[100]),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Center(
              child: Container(
                padding: new EdgeInsets.only(left: 32, right: 32),
                child: Text(
                  "Send your HappyNotes to cheering up those who in Isoman! 💗",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                ),
              ),
            ),
            SizedBox(
              height: 68,
            ),
            IconButton(
              icon: const Icon(
                Icons.menu,
                color: Colors.pink,
              ),
              onPressed: () => _scaffoldKey.currentState!.openDrawer(),
            ),
            SizedBox(
              height: 28,
            ),
            // cards view
            Expanded(
              flex: 2,
              child: ListView(
                children: [],
              ),
            )
          ]),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
        ),
        onPressed: () => setState(() {}),
        hoverColor: Colors.black,
        tooltip: 'Add note',
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
      ),
    );
  }
}
