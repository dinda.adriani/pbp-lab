// ignore_for_file: unnecessary_const

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7/widget/drawer.dart';
import 'package:lab_7/widget/form.dart';
import 'package:lab_7/widget/note.dart';

class NotesPage extends StatelessWidget {
  NotesPage({Key? key}) : super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {
            if (_scaffoldKey.currentState!.isDrawerOpen == false) {
              _scaffoldKey.currentState!.openDrawer();
            } else {
              _scaffoldKey.currentState!.openEndDrawer();
            }
          },
        ),
        title: const Text("Temenin Isoman"),
      ),
      body: Scaffold(
          key: _scaffoldKey,
          drawer: const MainDrawer(),
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.only(
                  top: 20, bottom: 20, left: 10, right: 10),
              margin: const EdgeInsets.all(8),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                        "Send your Happy Notes to cheering up those who in Isoman :)!"),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      height: 50.0,
                      decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.pink.withOpacity(0.1),
                            blurRadius: 1,
                            offset: Offset(10, 10),
                          ),
                        ],
                      ),
                      child: RaisedButton(
                        elevation: 30,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0),
                            side: BorderSide(color: Colors.pink)),
                        onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) {
                          return const AddForm();
                        }));},
                        padding: EdgeInsets.all(10.0),
                        color: Colors.pink,
                        textColor: Colors.white,
                        child: Text("Add Note!".toUpperCase(),
                            style: TextStyle(fontSize: 15)),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    GridView.count(
                      shrinkWrap: true,
                      controller: _scrollController,
                      crossAxisCount: 1,
                      crossAxisSpacing: 8,
                      mainAxisSpacing: 8,
                      children: <Widget>[
                        Note(),
                        Note(),
                      ],
                    )
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
