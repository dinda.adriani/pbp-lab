import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddForm extends StatefulWidget {
  const AddForm({Key? key}) : super(key: key);

  @override
  AddFormState createState () {
    return AddFormState();
  }
}

class AddFormState extends State<AddForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  validator: (value){
                    if(value==null||value.isEmpty){
                      return 'Please enter name/initials/aliases';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: 'Name/initials/aliases'
                  ),
                ),
                TextFormField(
                  validator: (value){
                    if(value==null||value.isEmpty){
                      return 'Please enter title of the message';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      labelText: 'Title'
                  ),
                ),
                TextFormField(
                  validator: (value){
                    if(value==null||value.isEmpty){
                      return 'Please enter message';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      labelText: 'Your Message'
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: (){
                      if(_formKey.currentState!.validate()){
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text('Processing Data')));
                      }
                    },
                    child: const Text('Submit')
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
