import 'package:flutter/cupertino.dart';

class NoteCard extends StatelessWidget {
  final String? title;
  final String? sender;
  final String? receiver;
  final String? message;

  const NoteCard(
      {Key? key, this.title, this.sender, this.receiver, this.message})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
    );
  }
}
