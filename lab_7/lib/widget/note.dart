import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Note extends StatelessWidget {
  const Note({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25)),
          child: Container(
            padding: const EdgeInsets.all(16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: const <Widget>[
                ListTile(
                  title: Text(
                    'Be happy',
                    textAlign: TextAlign.center,
                  ),
                  subtitle: Text('from: lalalilili'),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam iaculis et dolor at scelerisque. Cras laoreet luctus ante, vel interdum urna tempus eget. Nulla augue justo, laoreet ac erat iaculis")
              ],
            ),
          )),
    );
  }
}