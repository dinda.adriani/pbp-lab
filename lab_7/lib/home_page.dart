import 'package:flutter/material.dart';
import 'package:lab_7/widget/drawer.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {
            if (_scaffoldKey.currentState!.isDrawerOpen == false) {
              _scaffoldKey.currentState!.openDrawer();
            } else {
              _scaffoldKey.currentState!.openEndDrawer();
            }
          },
        ),
        title: Text(widget.title),
      ),
      body: Scaffold(
        key: _scaffoldKey,
        drawer: const MainDrawer(),
        body: const Center(
          child: Text("This is home page. Please go to the Happy Notes page"),
        ),
      ),
    );
  }
}
