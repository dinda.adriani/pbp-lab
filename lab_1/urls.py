from django.urls import path
from .views import friend_list, index

urlpatterns = [
    path('', index, name='index'), # ke views bagian index
    path('friends', friend_list, name='friends') # ke views bagian friends
]
