from django.shortcuts import render
from lab_1.models import Friend
from datetime import datetime, date


mhs_name = 'Dinda Adriani Siregar'  
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 12, 19)  # TODO Implement this, format (Year, Month, Date)
npm = 2006528622  


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all() # ngambil data dari models friend
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
