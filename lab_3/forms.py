from django.db import models
from django.db.models import fields
from django.forms import widgets
from lab_1.models import Friend
from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'

class FriendForm(forms.ModelForm):
    class Meta: 
        model = Friend
        fields = '__all__'
        widgets = {'birth_date': DateInput()}


