from django.shortcuts import redirect, render
from datetime import datetime, date
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required

# Create your views here.
# agar kalau add friend harus login dulu
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all() # ngambil data dari models friend
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        if request.method == "POST" :
            return redirect('/lab-3/') 
        # supaya habis submit balik lagi ke friend list
  
    context['form']= form
    return render(request, "lab3_forms.html", context)